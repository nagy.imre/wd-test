<?php
namespace storage_test;
/**
 * Class Brand
 * @package storage_test
 */
class Brand
{
    /**
     * Basic attribute name
     *
     * @var string
     */
    private $name;
    /**
     * Quality category
     *
     * @var int
     */
    private $qualityCategory;
    /**
     * Allowed categories
     *
     * @var array
     */
    private $allowedCategories = [1,2,3,4,5];

    /**
     * Debug function
     *
     * @return string
     */
    public function __toString()
    {
        $allowedCategoriesCategory = null;
        foreach ($this->allowedCategories as $category) {
            $allowedCategoriesCategory .= $category . ',';
        }
        return "Debug message from Brand Class :\n Name = " . $this->getName() . ", Quality category = " . $this->getQualityCategory() . ", Allowed categories [" . $allowedCategoriesCategory . "]";
    }

    /**
     * Return brand name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the brand name
     *
     * @param string $name
     * @return Brand
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Return brand Quality category
     *
     * @return int
     */
    public function getQualityCategory()
    {
        return (int)$this->qualityCategory;
    }

    /**
     * Set the brand Quality category
     *
     * @param int $qualityCategory
     * @return Brand
     */
    public function setQualityCategory($qualityCategory)
    {
        $this->qualityCategory = (int)$qualityCategory;
        return $this;
    }
    /**
     * Return brand allowed categories
     *
     * @return array
     */
    public function getAllowedCategories()
    {
        return $this->allowedCategories;
    }
}