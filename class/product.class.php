<?php
namespace storage_test;

include_once CLASS_DIR."brand.class.php";

/**
 * Class Product
 *
 * @package storage_test
 */
class Product
{
    /**
     * Product identifier string
     *
     * @var string
     */
    private $articleNumber;
    /**
     * Basic attribute name
     *
     * @var string
     */
    private $name;
    /**
     * Basic attribute price
     *
     * @var float
     */
    private $price;
    /**
     * Brand object, product has a brand
     *
     * @var object
     */
    private $brand;
    /**
     * Extra attribute array
     *
     * @var array
     */
    private $attributes = [];

    /**
     * Debug function
     *
     * @return string
     */
    public function __toString()
    {
        $brandToString = (is_object($this->brand) && method_exists($this->brand,'__toString'))? $this->brand->__toString(): null;
        return "Debug message from Product Class :\n\n Article number = " . $this->getArticleNumber() . ", Name = " . $this->getName() . ", Price = " . $this->getPrice().", Brand = \n\n".$brandToString;
    }
    /**
     * Return product article number
     *
     * @return string
     */
    public function getArticleNumber()
    {
        return $this->articleNumber;
    }

    /**
     * Set product article number
     *
     * @param string $articleNumber
     * @return Product
     */
    public function setArticleNumber($articleNumber)
    {
        $this->articleNumber = $articleNumber;
        return $this;
    }

    /**
     * Return product name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the product name
     *
     * @param string $name
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Return product price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set the product price
     *
     * @param float $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = floatval($price);
        return $this;
    }

    /**
     * Return product attributes
     *
     * @return array Product
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Set the product attributes
     *
     * @param array $attributes
     * @return Product
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    /**
     * Return product brand
     *
     * @return object Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set the product brand
     *
     * @param object $brand
     * @return Product
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

}