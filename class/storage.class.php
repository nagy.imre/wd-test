<?php
namespace storage_test;

include_once CLASS_DIR."product.class.php";

/**
 * Class Storage
 *
 * @package storage_test
 */
class Storage
{
    /**
     * Basic attribute name
     *
     * @var string
     */
    private $name;
    /**
     * Basic attribute address
     *
     * @var string
     */
    private $address;
    /**
     * Maximal capacity
     *
     * @var int
     */
    private $sumCapacity;
    /**
     * Product object array, storage has products
     *
     * @var array
     */
    private $products = [];

    /**
     * Debug message from the class
     *
     * @return string
     */
    public function __toString()
    {
        $productToString = null;
        if(is_array($this->products)) {
            $productToString .= "\n";
            $productToString .= "products => [";
            foreach ($this->products as $product) {
                if (isset($product) && is_object($product) && method_exists($product,'__toString')) {
                    $productToString .= "\n";
                    $productToString .= "[" . $product->__toString() . "], ";
                }
            }
            $productToString .= "]";
            $productToString .= "\n";
            $productToString .= "quantity => [" . count($this->products) . "],";
        }
        return "Debug message from Storage Class :\n\n Name = " . $this->getName() . ", Address = " . $this->getAddress() . ", Sum capacity = " . $this->getSumCapacity().", Products = \n\n".$productToString;
    }
    /**
     * Return storage name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set storage name
     *
     * @param string $name
     * @return Storage
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Return storage address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set storage address
     *
     * @param $address
     * @return Storage
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Return storage sum capacity
     *
     * @return int
     */
    public function getSumCapacity()
    {
        return (int)$this->sumCapacity;
    }

    /**
     * Set storage sum capacity
     *
     * @param int $sumCapacity
     * @return Storage
     */
    public function setSumCapacity($sumCapacity)
    {
        $this->sumCapacity = (int)$sumCapacity;
        return $this;
    }

    /**
     * Return storage products
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Add a product to the products array
     *
     * @param object $product
     * @return bool
     */
    public function addProduct($product)
    {
        $return = false;

        if (is_object($product) && method_exists($product,'getArticleNumber') && method_exists($product,'getName')) {
            if ($this->getSumCapacity() > count($this->products)) {
                $this->products[] = $product;
                $return = true;
            } else {
                // too much products
                $return = false;

            }
        }
        return $return;
    }

    /**
     * Remove product from the products array
     *
     * @param $articleNumber
     * @return bool
     */
    public function removeProduct($articleNumber)
    {
        $articleNumber = trim($articleNumber);
        $return = false;
        if (strlen($articleNumber)) {
            foreach ($this->products as $key => &$product) {
                if ($product->getArticleNumber() === $articleNumber ) {
                    unset($this->products[$key]);
                    $return = true;
                    break;
                }
            }
            unset($product);
        }
        return $return;
    }

}