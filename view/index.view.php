<header class="bg-primary text-white">
    <div class="container text-center">
        <h1>Raktár teszt alkalmazás</h1>
        <p class="lead">Egy Bootstrap 4 alapú templaten</p>
    </div>
</header>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2>Legfontosabb funkciók</h2>
                <p class="lead">Szimulálom a következő eseteket:</p>
                <ul>
                    <li><a href="<?php echo LINK; ?>first-simulation">Első szimuláció:</a> Létrehoz 2 raktárat, felvesz x terméket, kikér y terméket</li>
                    <li><a href="<?php echo LINK; ?>second-simulation">Második szimuláció:</a> Létrehoz 2 raktárat, felvesz x terméket, de nincs elég hely a raktárban</li>
                    <li><a href="<?php echo LINK; ?>third-simulation">Harmadik szimuláció:</a> Létrehoz 2 raktárat, felvesz x terméket, majd kikér többet, mint amennyi elérhető</li>
                </ul>
            </div>
        </div>
    </div>
</section>
