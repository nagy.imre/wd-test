<section id="simulation">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="alert-warning">Figyelem: A szimulácó előtt a raktár alapállapotba állítottam</div>
                <h2>Az II. Szimuláció eredménye</h2>
                <h3>Szimuláltam a következő esetet:</h3>
                <p class="lead">Létrehoztam 2 raktárat, felvettem x terméket, de nincs elég hely a raktárban</p>
                <h3>Felvinni kívánt cikkszámok:</h3>
                <ol>
                    <?php
                    if (count($_SESSION[TEST_DATA]['simulation2'])) {
                        foreach ($_SESSION[TEST_DATA]['simulation2'] as $simulation1) {
                            echo '<li>' . $simulation1['articleNumber'] . '</li>';
                        }
                    }
                    ?>
                </ol>
                <h3>Mit csinálok?</h3>
                <p class="lead">Megpróbálom túltölteni a raktárakat, de várhatóan hibát kapok!</p>
                <h3>Raktárak aktuális állapota:</h3>
                <?php
                include_once VIEW_DIR . 'storage.list.view.php';
                ?>
                <div class="alert-success">A szimluáció eredménye megmarad. Tekintse meg <a href="<?php echo LINK; ?>list-storage" title="Az aktuális lista lekérése">a raktár lista</a> menüpontot.</div>
            </div>
        </div>
    </div>
</section>
