<!DOCTYPE html>
<html lang="hu">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Raktár Teszt Applikáció</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo STATIC_DIR; ?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo STATIC_DIR; ?>css/scrolling-nav.css" rel="stylesheet">

</head>

<body id="page-top">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand" href="<?php echo LINK; ?>">Főoldal</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo LINK; ?>first-simulation">Első szimuláció</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo LINK; ?>second-simulation">Második szimuláció</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo LINK; ?>third-simulation">Harmadik szimuláció</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?php echo LINK; ?>list-storage">Raktár lista</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<?php
if (count($storageController->getErrorMessages())) {
    ?>
<section id="errors" class="bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2>Hiba történt futás közben</h2>
                <h3>Üzenetek</h3>
                <ul>
                    <?php
                    if (count($storageController->getErrorMessages())) {
                        foreach ($storageController->getErrorMessages() as $error) {
                            echo '<li class="alert-danger">' . $error . '</li>';
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</section>
<?php
}
?>
<?php
include_once VIEW_DIR . $storageController->getTemplate() .'.view.php';
?>
<section id="services" class="bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2>Rendszer üzenetek</h2>
                <h3>Lefutott függvények</h3>
                <ul>
                    <?php
                    if (count($storageController->getMessages())) {
                        foreach ($storageController->getMessages() as $message) {
                            echo '<li>' . $message . '</li>';
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Nagy Imre 2017</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo STATIC_DIR; ?>js/jquery-3.2.1.min.js"></script>
<script src="<?php echo STATIC_DIR; ?>js/popper.min.js"></script>
<script src="<?php echo STATIC_DIR; ?>js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="<?php echo STATIC_DIR; ?>js/jquery.easing.min.js"></script>

<!-- Custom JavaScript for this theme -->
<script src="<?php echo STATIC_DIR; ?>js/scrolling-nav.js"></script>

</body>

</html>