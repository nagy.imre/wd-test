<section id="simulation">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="alert-warning">Figyelem: A szimulácó előtt a raktár alapállapotba állítottam</div>
                <h2>Az I. Szimuláció eredménye</h2>
                <h3>Szimuláltam a következő esetet:</h3>
                <p class="lead">Létrehoztam 2 raktárat, felvettem x terméket, kikértem y terméket</p>
                <h3>Felvett cikkszámok:</h3>
                <ol>
                    <?php
                    $randomDelete = (int)$_SESSION[TEST_DATA]['simulation1_random_delete'];
                    if (count($_SESSION[TEST_DATA]['simulation1'])) {
                        foreach ($_SESSION[TEST_DATA]['simulation1'] as $simulation1) {
                            echo '<li>' . $simulation1['articleNumber'] . '</li>';
                        }
                    }
                    ?>
                </ol>
                <h3>Mit csinálok?</h3>
                <p class="lead">Törlök egy <b><?php echo $_SESSION[TEST_DATA]['simulation1'][$randomDelete]['articleNumber']; ?></b>  elemet!</p>
                <h3>Raktárak aktuális állapota:</h3>
                <?php
                include_once VIEW_DIR . 'storage.list.view.php';
                ?>
                <div class="alert-success">A szimluáció eredménye megmarad. Tekintse meg <a href="<?php echo LINK; ?>list-storage" title="Az aktuális lista lekérése">a raktár lista</a> menüpontot.</div>
            </div>
        </div>
    </div>
</section>
