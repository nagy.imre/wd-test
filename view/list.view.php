<section id="storages">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <h2>Raktár lista</h2>
                <?php
                include_once VIEW_DIR . 'storage.list.view.php';
                ?>
            </div>
        </div>
    </div>
</section>
