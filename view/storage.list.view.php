<ul>
    <?php
    if (is_object($storageController->getStorage1())) {
        echo '<li class="storage-name"> ' . $storageController->getStorage1()->getName() . '</li>';
        echo '<li><label>Cím:</label> ' . $storageController->getStorage1()->getAddress() . '</li>';
        echo '<li><label>Max. termékszám:</label> ' . $storageController->getStorage1()->getSumCapacity() . '</li>';
        echo '<li><h3>Termékek:</h3> ';
        echo '<ol>';
        if (is_array($storageController->getStorage1()->getProducts())) {
            foreach ($storageController->getStorage1()->getProducts() as $product) {
                echo '<li>';
                echo '<ul>';
                echo '<li><label>Cikszám:</label> ' . $product->getArticleNumber() . '</li>';
                echo '<li><label>Név:</label> ' . $product->getName() . '</li>';
                echo '<li><label>Ár:</label> ' . $product->getPrice() . 'Ft</li>';
                if (is_array($product->getAttributes())) {
                    echo '<li><label>Attribútomok:</label> ' . implode(' ',$product->getAttributes()) . '</li>';
                }
                if (is_object($product->getBrand())) {
                    echo '<li><label>Márkanév:</label> ' . $product->getBrand()->getName() . '</li>';
                    echo '<li><label>Kategória:</label> ' . $product->getBrand()->getQualityCategory() . '</li>';
                }
                echo '</ul>';
                echo '</li>';
            }
        }
        echo '</ol>';
        echo '</li>';
    }
    ?>
</ul>
<ul>
    <?php
    if (is_object($storageController->getStorage2())) {
        echo '<li class="storage-name"> ' . $storageController->getStorage2()->getName() . '</li>';
        echo '<li><label>Név:</label> ' . $storageController->getStorage2()->getName() . '</li>';
        echo '<li><label>Cím:</label> ' . $storageController->getStorage2()->getAddress() . '</li>';
        echo '<li><label>Max. termékszám:</label> ' . $storageController->getStorage2()->getSumCapacity() . '</li>';
        echo '<li><h3>Termékek:</h3> ';
        echo '<ol>';
        if (is_array($storageController->getStorage2()->getProducts())) {
            foreach ($storageController->getStorage2()->getProducts() as $product) {
                echo '<li>';
                echo '<ul>';
                echo '<li><label>Cikszám:</label> ' . $product->getArticleNumber() . '</li>';
                echo '<li><label>Név:</label> ' . $product->getName() . '</li>';
                echo '<li><label>Ár:</label> ' . $product->getPrice() . 'Ft</li>';
                if (is_array($product->getAttributes())) {
                    echo '<li><label>Attribútomok:</label> ' . implode(' ',$product->getAttributes()) . '</li>';
                }
                if (is_object($product->getBrand())) {
                    echo '<li><label>Márkanév:</label> ' . $product->getBrand()->getName() . '</li>';
                    echo '<li><label>Kategória:</label> ' . $product->getBrand()->getQualityCategory() . '</li>';
                }
                echo '</ul>';
                echo '</li>';
            }
        }
        echo '</ol>';
        echo '</li>';
    }
    ?>
</ul>