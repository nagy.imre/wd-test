<section id="simulation">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 mx-auto">
                <div class="alert-warning">Figyelem: A szimulácó előtt a raktár alapállapotba állítottam</div>
                <h2>Az III. Szimuláció eredménye</h2>
                <h3>Szimuláltam a következő esetet:</h3>
                <p class="lead">Létrehoztam 2 raktárat, felvettem x terméket, majd kikértem többet, mint amennyi elérhető</p>
                <h3>Mit csinálok?</h3>
                <p class="lead">Kiürítem a raktárat, és megpróbálom túl sok elemet kivenn, miután elfogytak az elemek és nincs több termék hibát kapok, mert nem létező terméket akarok kiszedni!</p>

                <h3>Felvinni kívánt cikkszámok:</h3>
                <ol>
                    <?php
                    if (count($_SESSION[TEST_DATA]['simulation3_add'])) {
                        foreach ($_SESSION[TEST_DATA]['simulation3_add'] as $simulation1) {
                            echo '<li>' . $simulation1['articleNumber'] . '</li>';
                        }
                    }
                    ?>
                </ol>
                <h3>Törölni kívánt cikkszámok:</h3>
                <ol>
                    <?php
                    if (count($_SESSION[TEST_DATA]['simulation3_remove'])) {
                        foreach ($_SESSION[TEST_DATA]['simulation3_remove'] as $simulation1) {
                            echo '<li>' . $simulation1['articleNumber'] . '</li>';
                        }
                    }
                    ?>
                </ol>
                <h3>Raktárak aktuális állapota:</h3>
                <?php
                include_once VIEW_DIR . 'storage.list.view.php';
                ?>
                <div class="alert-success">A szimluáció eredménye megmarad. Tekintse meg <a href="<?php echo LINK; ?>list-storage" title="Az aktuális lista lekérése">a raktár lista</a> menüpontot.</div>
            </div>
        </div>
    </div>
</section>
