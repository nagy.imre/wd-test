<?php


include_once CLASS_DIR."storage.class.php";

/**
 * Class StorageController
 */
class StorageController
{
    /**
     * Storage copies
     *
     * @var object
     */
    private $storage1;
    private $storage2;

    /**
     * Function basic messages array
     *
     * @var array
     */
    private $messages = [];

    /**
     * Function basic messages array
     *
     * @var array
     */
    private $errors = [];

    /**
     * Template name, used by the functions
     *
     * @var string
     */
    private $template;

    public function __construct()
    {
        if (!isset($_SESSION[STORE_OBJECT]['storage1']) || !isset($_SESSION[STORE_OBJECT]['storage1'])) {
            //create storage
            $this->createStorage();
        }else{
            //restore storage
            $this->restoreStorage();
        }
    }

    public function __destruct()
    {
        //save storage data
        $this->saveStorage();
    }

    /**
     * Debug function
     *
     * @return string
     */
    public function __toString()
    {
        return "Debug message from StorageController\n\n Class Object: storage1, storage2, messages, errors  template\n\n Functions  __construct, __destruct, index, listStorage, firstSimulation, secondSimulation, thirdSimulation, debug, getStorage1, getStorage2, getMessages, addMessage, getErrorMessages, addErrorMessages, setTemplate, getTemplate,  createStorage, restoreStorage, saveStorage, addProductToStorage, removeProductToStorage";
    }

    /**
     * Index function, this generates the default landing page
     */
    public function index()
    {
        $this->addMessage('index()');
        $this->setTemplate('index');
    }

    /**
     * Storage list
     */
    public function listStorage()
    {
        $this->addMessage('listStorage()');
        $this->setTemplate('list');

    }

    /**
     * First simulation, put x product and remove  y. It based on the config variables
     */
    public function firstSimulation()
    {
        //need reset storage for simulation
        $this->createStorage();
        $randomDelete = (int)$_SESSION[TEST_DATA]['simulation1_random_delete'];

        //put x product and remove  y
        $this->addMessage('firstSimulation()');
        try {
            //add some test products
            foreach ($_SESSION[TEST_DATA]['simulation1'] as $simulation) {
                $this->addProductToStorage($simulation['brandId'], $simulation['articleNumber']);
            }
            //just for test delete
            $this->removeProductFromStorage($_SESSION[TEST_DATA]['simulation1'][$randomDelete]['articleNumber']);

        } catch (Exception $e) {
            $this->addErrorMessage($e->getMessage());
        }
        $this->setTemplate('first');

    }

    /**
     * Second simulation, put x product to the storage but there is not enough space. It based on the config variables
     */
    public function secondSimulation()
    {
        //need reset storage for simulation
        $this->createStorage();
        //put x product to the storage but there is not enough space
        $this->addMessage('secondSimulation()');
        try {
            //add some test products but it will be too much
            foreach ($_SESSION[TEST_DATA]['simulation2'] as $simulation) {
                $this->addProductToStorage($simulation['brandId'], $simulation['articleNumber']);
            }

        } catch (Exception $e) {
            //yes too much, as I expected...
            $this->addErrorMessage($e->getMessage());
        }

        $this->setTemplate('second');
    }

    /**
     * Third simulation, put x product to the storage but remove more than avalible. It based on the config variables
     */
    public function thirdSimulation()
    {
        //need reset storage for simulation
        $this->createStorage();
        //put x product to the storage but remove more than avalible
        $this->addMessage('thirdSimulation()');
        try {
            //add some test products
            foreach ($_SESSION[TEST_DATA]['simulation3_add'] as $simulation) {
                $this->addProductToStorage($simulation['brandId'], $simulation['articleNumber']);
            }
            //remove more than we avalible
            foreach ($_SESSION[TEST_DATA]['simulation3_remove'] as $simulation) {
                $this->removeProductFromStorage($simulation['articleNumber']);
            }

        } catch (Exception $e) {
            $this->addErrorMessage($e->getMessage());
        }

        $this->setTemplate('third');
    }

    /**
     * Debug message generator
     */
    public function debug()
    {
        $this->addMessage('debug()');
        $this->setTemplate('debug');
    }

    /**
     * Return storage1 object
     *
     * @return object
     */
    public function getStorage1()
    {
        return $this->storage1;
    }

    /**
     * Return storage2 object
     *
     * @return object
     */
    public function getStorage2()
    {
        return $this->storage2;
    }

    /**
     * Return template name
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }
    /**
     * Return messages array
     *
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }
    /**
     * Put a message into the messages array
     *
     * @param $message
     */
    private function addMessage($message)
    {
        $message = trim($message);
        if (strlen($message)) {
            $this->messages[] = $message;
        }
    }
    /**
     * Return error messages array
     *
     * @return array
     */
    public function getErrorMessages()
    {
        return $this->errors;
    }
    /**
     * Put an error message into the messages array
     *
     * @param $error
     */
    private function addErrorMessage($error)
    {
        $error = trim($error);
        if (strlen($error)) {
            $this->errors[] = $error;
        }
    }
    /**
     * Set template name based on the $template variable
     *
     * @param $template
     * @return $this
     */
    private function setTemplate($template)
    {
        $this->template = $template;
        return $this;
    }

    /**
     * Create storage objects
     */
    private function createStorage()
    {
        $this->addMessage('createStorage()');

        $this->storage1 = new \storage_test\Storage();
        $this->storage2 = new \storage_test\Storage();

        //usually it comes from user input, but now it's just a test
        $this->storage1->setName($_SESSION[TEST_DATA]['storageData'][0]['name']);
        $this->storage1->setAddress($_SESSION[TEST_DATA]['storageData'][0]['address']);
        $this->storage1->setSumCapacity($_SESSION[TEST_DATA]['storageData'][0]['sumCapacity']);

        $this->storage2->setName($_SESSION[TEST_DATA]['storageData'][1]['name']);
        $this->storage2->setAddress($_SESSION[TEST_DATA]['storageData'][1]['address']);
        $this->storage2->setSumCapacity($_SESSION[TEST_DATA]['storageData'][1]['sumCapacity']);

    }

    /**
     * Restore storage objects
     */
    private function restoreStorage()
    {
        $this->addMessage('restoreStorage()');
        //usually it comes from a database
        $this->storage1 = $_SESSION[STORE_OBJECT]['storage1'];
        $this->storage2 = $_SESSION[STORE_OBJECT]['storage2'];

    }

    /**
     * Save storage objects
     */
    private function saveStorage()
    {
        $this->addMessage('saveStorage()');
        //usually it saves into the database
        $_SESSION[STORE_OBJECT]['storage1'] = $this->storage1;
        $_SESSION[STORE_OBJECT]['storage2'] = $this->storage2;

    }

    /**
     * Add one product into one of the storage
     * It can throw exceptions
     *
     * @param $brandId
     * @param $articleNumber
     * @throws Exception
     * @return bool
     */
    private function addProductToStorage($brandId, $articleNumber)
    {
        $this->addMessage('addProductToStorage()');
        $return = false;
        $articleNumber = trim($articleNumber);
        $brandId = (int)$brandId;

        $insertProductData = null;
        $insertBrandData = null;
        $product = null;
        $brand = null;

        if (in_array($brandId,[0,1])) {
            if (strlen($articleNumber)) {

                if (isset($_SESSION[TEST_DATA]['brandData'][$brandId])) {
                    $insertBrandData = $_SESSION[TEST_DATA]['brandData'][$brandId];
                    //if it comes form user input I should put some validation here but now it comes from the config file

                    if (isset($_SESSION[TEST_DATA]['productData'][$articleNumber])) {
                        $insertProductData = $_SESSION[TEST_DATA]['productData'][$articleNumber];

                        $product = new \storage_test\Product();
                        $brand = new \storage_test\Brand();

                        $brand->setName($insertBrandData['name']);
                        $brand->setQualityCategory($insertBrandData['qualityCategory']);

                        //if it comes form user input I should put some validation here but now it comes from the config file
                        $product->setArticleNumber($insertProductData['articleNumber']);
                        $product->setName($insertProductData['name']);
                        $product->setPrice($insertProductData['price']);
                        $product->setAttributes($insertProductData['attributes']);
                        $product->setBrand($brand);

                        //add product storage 1 or 2 if it fails then error

                        if ($this->storage1->getSumCapacity() > count($this->storage1->getProducts())) {
                            //add to the first storage
                            $return = $this->storage1->addProduct($product);
                        } elseif ($this->storage2->getSumCapacity() > count($this->storage2->getProducts())) {
                            //add to the second storage
                            $return = $this->storage2->addProduct($product);
                        } else {
                            //Missing product
                            throw new Exception('Mindkét raktár megtelt! A ' . $articleNumber . ' cikkszámú termék már nem fér el!!');
                        }

                    } else {
                        //Missing product
                        throw new Exception('Rossz cikkszám! ' . $articleNumber . ' Nincs ilyen termék!');
                    }
                } else {
                    //Missing brand
                    throw new Exception('Nincs ilyen márka!');
                }
            } else {
                //Wrong input for product
                throw new Exception('Az articleNumber változó nem lehet üres!');
            }

        } else {
            //Wrong brand id
            throw new Exception('Rossz márka azonosító csak 0 vagy 1 lehet.');
        }

        return $return;
    }

    /**
     * Remove an element from one of the storage
     *
     * @param $articleNumber
     * @throws Exception
     * @return bool
     */
    private function removeProductFromStorage($articleNumber)
    {
        $return = false;
        $this->addMessage('removeProductFromStorage()');
        $articleNumber = trim($articleNumber);

        if (strlen($articleNumber)) {
            //remove from storage 1 or 2
            if ($this->storage1->getSumCapacity() !== 0 || $this->storage2->getSumCapacity() !== 0 ) {
                if ($this->storage1->getSumCapacity() > 0 && $this->storage1->removeProduct($articleNumber)) {
                    $return = true;
                } elseif ($this->storage2->getSumCapacity() > 0 && $this->storage2->removeProduct($articleNumber)) {
                    $return = true;
                } else {
                    throw new Exception('Rossz cikkszám! ' . $articleNumber . ' Nincs ilyen termék!');
                }

            } else {
                //Wrong input for product
                throw new Exception('Mindkét raktár üres!');
            }
        } else {
            //Wrong input for product
            throw new Exception('Az articleNumber változó nem lehet üres!');
        }
        return $return;
    }

}
