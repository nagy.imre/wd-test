<?php
//error_reporting(E_ALL & ~E_NOTICE);

define('CLASS_DIR',BASE_DIR.'/class/');
define('VIEW_DIR',BASE_DIR.'/view/');
define('CONTROLLER_DIR',BASE_DIR.'/controller/');

define('LINK','/wd-test/');
define('STATIC_DIR',LINK.'/static/');

define('TEST_DATA','testData');
define('STORE_OBJECT','testObjects');

//default template
define('BASE_VIEW','app.view.php');

include_once BASE_DIR.'/controller/storage.controller.php';

session_start();

//some test data for brands
$_SESSION[TEST_DATA]['brandData'] = [
    [
        'name' => 'Test brand 1',
        'qualityCategory' => 1
    ],
    [
        'name' => 'Test brand 2',
        'qualityCategory' => 2
    ]
];
//some test data for products
$_SESSION[TEST_DATA]['productData'] = [
    '1234AS' => [
        'articleNumber' => '1234AS',
        'name' => 'Test product 1',
        'price' => 10,
        'attributes' => [
            'color' => 'zöld',
        ]
    ],
    '5678DF' => [
        'articleNumber' => '5678DF',
        'name' => 'Test product 1',
        'price' => 12.5,
        'attributes' => [
            'weight' => 5,
            'weightType' => 'Kg',
        ]
    ],
];
//some test data for storage
$_SESSION[TEST_DATA]['storageData'] = [
    [
        'name' => 'Test storage 1',
        'address' => 'Budapest, 1092 Lorem ipsum utca 10/b',
        'sumCapacity' => 2,
    ],
    [
        'name' => 'Test storage 2',
        'address' => 'Budapest, 1122 Dolor utca 7',
        'sumCapacity' => 3,
    ]
];

$_SESSION[TEST_DATA]['simulation1'] = [
    [
        'articleNumber' => '1234AS',
        'brandId' => 0
    ],
    [
        'articleNumber' => '1234AS',
        'brandId' => 1
    ],
    [
        'articleNumber' => '5678DF',
        'brandId' => 1
    ],
];
$_SESSION[TEST_DATA]['simulation1_random_delete'] = rand(0, 2);

$_SESSION[TEST_DATA]['simulation2'] = [
    [
        'articleNumber' => '1234AS',
        'brandId' => 0
    ],
    [
        'articleNumber' => '1234AS',
        'brandId' => 1
    ],
    [
        'articleNumber' => '5678DF',
        'brandId' => 1
    ],
    [
        'articleNumber' => '1234AS',
        'brandId' => 0
    ],
    [
        'articleNumber' => '1234AS',
        'brandId' => 1
    ],
    [
        'articleNumber' => '5678DF',
        'brandId' => 1
    ],
];

$_SESSION[TEST_DATA]['simulation3_add'] = [
    [
        'articleNumber' => '1234AS',
        'brandId' => 0
    ],
    [
        'articleNumber' => '1234AS',
        'brandId' => 1
    ],
];

$_SESSION[TEST_DATA]['simulation3_remove'] = [
    [
        'articleNumber' => '1234AS',
        'brandId' => 0
    ],
    [
        'articleNumber' => '1234AS',
        'brandId' => 1
    ],
    [
        'articleNumber' => '1234AS',
        'brandId' => 0
    ],
];