<?php
define('BASE_DIR',DIRNAME(__FILE__));

include_once BASE_DIR.'/config/config.php';

$storageController = new StorageController();

if (isset($_GET['controller'])) {
    switch($_GET['controller']) {
        case 'first-simulation':
            $storageController->firstSimulation();
            break;
        case 'second-simulation':
            $storageController->secondSimulation();
            break;
        case 'third-simulation':
            $storageController->thirdSimulation();
            break;
        case 'list-storage':
            $storageController->listStorage();
            break;
        case 'debug':
            $storageController->debug();
            break;
        default:
            $storageController->index();
            break;
    }
} else {
    $storageController->index();
}
include_once VIEW_DIR . BASE_VIEW;